# 知识库

> 构建自己的知识库，高效便捷的知识管理工具

## 💡简介

这是一个集笔记、动态和待办为一体的插件，功能强大的笔记功能，简单易用的待办功能，可以使你很方便的管理你的笔记内容。可以通过多种主题自定义插件样式，可以通过插件拓展编辑器功能

## 相关链接

### 使用

- [utools](https://u.tools/plugins/detail/知识库/)
- [chrome](https://chromewebstore.google.com/detail/知识库/dijccegekgmhjfblcbgjbnkhffegaiel)

## 📝笔记

支持富文本笔记，markdown笔记、表格笔记、思维导图和代码笔记。

可以编写自己的Markdown笔记进行保存。

也可以借助插件【听雨Html转Markdown】，将网络上的笔记快速剪藏到知识库中。

## ✅待办

插件提供一个简单实用的待办功能，满足您日常的待办使用，功能上简介高效。

## 👍特色功能

1. 备份功能   
   本插件支持将全部数据备份到WebDAV和本地，方便您保存历史数据用于恢复。
2. 多种笔记类型支持  
   支持富文本笔记，markdown笔记、表格笔记和代码笔记，总有一个适合你
3. utools  
   可以在搜索面板中直接搜索笔记，但需要设置搜索面板为聚合搜索，并在插件应用设置中设置为：允许推送内容到搜索面板。
4. 支持插件与主题  
   可以通过多种主题自定义插件样式，可以通过插件拓展编辑器功能
